/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Modelo.Punto;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class Funcion {
    
    
    private Punto puntos[];

    public Funcion() {
    }

    
    public Funcion(String datos)
    {
        //los datos de la fila 
        String datosFilasPtos[]=datos.split(";");
        this.puntos=new Punto[datosFilasPtos.length];
        for(int j=0;j<this.puntos.length;j++)
        {
            try {
                this.puntos[j]=new Punto(datosFilasPtos[j]);
            } catch (Exception ex) {
                System.out.println("Error al procesar:"+ex.getMessage());
            }
        }
                
       this.ordenarPuntos();
        
        
    }
    
    
    public Punto[] getPuntos() {
        return puntos;
    }

    public void setPuntos(Punto[] puntos) {
        this.puntos = puntos;
    }

    @Override
    public String toString() {
        String msg="";
        for(Punto datoPunto:this.puntos)
        {
            msg+=datoPunto.toString()+"\t";
        }
     return msg;
    }
    
    /**
     * Ordena los puntos de mayor a menor  (tomando en cuenta el eje X)
     * usando el método de ordenamiento selección.
     * Más información: 
     * https://www.ecured.cu/Algoritmo_de_ordenamiento_por_selecci%C3%B3n#:~:text=Algoritmo%20de%20ordenamiento%20por%20Selecci%C3%B3n%20(Selection%20Sort%20en%20ingl%C3%A9s)%3A,as%C3%AD%20sucesivamente%20hasta%20ordenarlo%20todo.
     * Advertencia: IMPLEMENTAR COMPARETO EN LA CLASE PUNTO
     */
    public void ordenarPuntos()
    {
            // :
    }
    
    
}
